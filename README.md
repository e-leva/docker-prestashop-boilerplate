# Una veloce quida a PrestaShop Docker Compose Starter Project

### Detailed Article can be found [here](https://dev.to/moghwan/setup-prestashop-with-docker-compose-39mn)

### SEGUIRE I SEGUENTO PASSAGGI:

Vai alla root della cartella del tuo progetto

Accedi con lo shell a quella cartella

```shell
git clone git@bitbucket.org:e-leva/docker-prestashop-boilerplate.git .
```
Personalizza i con i parametri corretti i file
.env
config/vhost.conf

in .env
sotituire tutti i valori delle variabile con le impostazione reali del progetto

in vhost.conf
sostituire NOME_DOMINIO con il nome del dominio del sito
e ALIAS_DOMINIO nel caso il sito gestisce più dominii (attenzione eliminare # dalla riga del comando)

Una volta terminato la configurazione, da shell

```shell
sh start.sh
```

Se tutto è andato a buon fine, modifica il tuo file .hosts come ti serve per accedere al tuo nuovo sito
Es. 127.0.0.1   dominio.ext

Se hai effettuato una installazione esistente da repository rinomina i file .htaccess.dist in .htaccess e app/config/parameters.php.dist in app/config/parameters.php
Personalizza se necessario il file .htaccess rispetto alle tue esigenze e verifica le informazioni del file parameters.php per accedere al database, 

altrimenti 

Se hai effettuato una nuova installazione, accedi da browser tramite il dominio del sito dominio.ext e segui le indicazioni di installazione di Prestashop


IN CASO DI ERRORI DURANTE L'INSTLLAZIONE DI PRESTASHOP

Se si presenta una errore di "grant write permission" da shell eseguire 
(cambia COMPOSE_PROJECT_NAME con il nome indicato nel file .env)

```shell
docker exec -ti COMPOSE_PROJECT_NAME_app_dev_1 bash
    chown www-data:www-data -R . && exit
```

Nello step di installazione del database utilizza i dati indicati sul file .env e come server address "mysql"

Se hai problemi di connesione puoi da shell creare un nuovo utente al database:
(cambia COMPOSE_PROJECT_NAME con il nome indicato nel file .env) 
(cambiare nelal query seguente il prefisso ps_ se indicato differentemente)

```shell
docker exec -ti COMPOSE_PROJECT_NAME_mysql_1 bash
    chown -R mysql:mysql /var/lib/mysql
    mysql -u root -p
    # password: root
    CREATE USER 'ps_user'@'%' IDENTIFIED BY 'user';
    GRANT ALL ON *.* TO 'ps_user'@'%';
    FLUSH PRIVILEGES;
    EXIT;
exit
```
In questo caso puoi utilizzare questo dati per acecdere al database nel passaggio di installazione di prestashop

* server: mysql
* database: prestashop
* user: ps_user
* password: user


PULIZIA POST INSTALLAZIONE

Quando l'installazione è terminata, fai un po' di pulizia con i segeunti comandi di shell
(cambia COMPOSE_PROJECT_NAME con il nome indicato nel file .env) 

```shell
docker exec -ti COMPOSE_PROJECT_NAME_app_dev_1 bash
    rm -rf install
    rm start.sh
    mv admin update
exit

docker-compose down --remove-orphans && docker-compose up -d



OK - Tutto fatto
