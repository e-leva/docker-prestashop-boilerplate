#!/bin/sh

# CARICA .ENV VARIABILI
#

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. "$DIR/.env"

# OUTPUT COLOR
#

RED='\033[0;31m'
NC='\033[0m' # No Color


# INSTALLAZIONE DOCKER
#


# VERIFICA DIPENDENZE DOCKER
#

composer -v > /dev/null 2>&1
COMPOSER=$?
if [[ $COMPOSER -ne 0 ]]; then
    echo
    echo ">>> ${RED}Composer risulta non Installato${NC} <<<"
    echo "Sequi le indicazioni alla seguente guida:"
    echo "https://getcomposer.org/"
    echo
    echo "Script teminato - installazione non effettuata"
    exit
fi

if !([ -x "$(command -v docker)" ];) then
    echo
    echo ">>> ${RED}Docker risulta non Installato${NC} <<<"
    echo "Sequi le indicazioni alla seguente guida:"
    echo "https://docs.docker.com/engine/install/ubuntu/"
    echo
    echo "Script teminato - installazione non effettuata"
    exit
fi

if !([ -x "$(command -v docker-compose)" ];) then
    echo
    echo ">>> ${RED}Docker Compose risulta non installato${NC} <<<"
    echo "Sequi le indicazioni alla seguente guida:"
    echo "https://phoenixnap.com/kb/install-docker-compose-on-ubuntu-20-04"
    echo
    echo "Script teminato - installazione non effettuata"
    exit
fi

# PROCEDURA AUTOMATOZZATA DOCKER
#

rm -rf .git

touch -a ./acme.json
chmod 600 ./acme.json

if [ -d "./prestashop" ]; then
    rm -rf prestashop
fi

while true; do
	read -p "Vuoi effettuare una installazione prestashop NUOVA? " yn

	case $yn in

        # INSTALLAZIONE DOCKER NUOVO
       	#

	[YySs]* )
			
		composer create-project prestashop/prestashop --prefer-dist

        mv prestashop public_html

        cp public_html/.htaccess public_html/.htaccess.dist
        cp public_html/app/config/parameters.php public_html/app/config/parameters.php.dist

        sh ./public_html/tools/assets/build.sh

		docker network create ${DEFAULT_NETWORK}

		docker-compose -f docker-compose.yml up -d --build

       		break;;


	# INSTALLAZIONE DOCKER DA PROGETTO ESISTENTE
        #

	[Nn]* )

		mkdir public_html
		cd public_html

		echo "Prego indicami il nome del repository da cui scaricare il progetto: "
		read name_repository

		git init
		git remote add origin git@bitbucket.org:e-leva/$name_repository.git

		git pull origin master

		composer install
		sh tools/assets/build.sh

		cd ..		

                docker network create ${DEFAULT_NETWORK}

                docker-compose -f docker-compose.yml up -d --build
		        
		exit;;


        * ) echo "${RED}Prego ripondere yes/si o no.!${NC}";;

    esac
done


echo "${RED}Fine esecuzione scritp!!!${NC}"